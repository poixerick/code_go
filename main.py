import gamelib
import playerlib
import numpy as np
import copy
import movelib
import matplotlib.pyplot as plt
import time

#jeu = gamelib.GameClass(5, 0.5)

#jeu.lance_gui()
jeu = gamelib.GameClass(5, 0.5)

nbCoup=[]
temps=[]
score=[]
nbSimu=[]

while (jeu.partie_finie != True):
    jeu.jouer(jeu.joueur_courant.donne_coup(jeu))
    if jeu.joueur_courant.values == 'MonteCarlo' :
        nbCoup.append(jeu.joueur_courant.nbCoupJoueTotal)
        temps.append(jeu.joueur_courant.timer)
        print(jeu.color)
        print(jeu.joueur_courant.meilleurScore)
        score.append(jeu.joueur_courant.meilleurScore * jeu.color)
        nbSimu.append(jeu.joueur_courant.nbSimulation)

plt.figure(figsize=(5, 5))
f,(a1,a2)=plt.subplots(1,2)
a1.plot(nbCoup,temps)
a1.set_title("stats MonteCarlo")
a1.set_xlabel("nombre de coup joué")
a1.set_ylabel("temps de reflexion")

a2.plot(nbSimu,score)
a2.set_title("stats MonteCarlo")
a2.set_xlabel("nombre de simulation")
a2.set_ylabel("score")
plt.show()