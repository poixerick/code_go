import random
import movelib
import copy
import numpy as np
import gamelib
import some_functions
import time
from abc import ABC, abstractmethod
from collections import defaultdict
import math


class Joueur():

	def __init__(self, jeu, couleur):
		self.jeu = jeu
		self.color = couleur

	def donne_coup(self):
		pass


class Humain(Joueur):
	values = 'Humain'
	pass


class IA(Joueur):
	pass


class Pass(IA):

	values = 'Pass'

	def donne_coup(self,game) :
		return -1

class Random(IA):

	values = 'Random'

	def donne_coup(self, game):
		while True :
			nbListeCoup = len(game.goban.liste_coups_oks()) - 1
			randomNumber = random.randint(0, nbListeCoup)
			coord = game.goban.liste_coups_oks()[randomNumber]
			if coord != -1 :
				if movelib.IsValidMove(coord,game.goban,game) :
					return coord
			else :
				return -1


def simulation(Game,coup,nbCoupJoueSimu):
	tab = []
	while True :
		coord = -1
		compteur = 0
		while coord == -1 :
			if nbCoupJoueSimu == 0 :
				coord = coup
			else :
				coord = Random.donne_coup('',Game)
			compteur += 1
			if compteur > 3 :
				s = Game.score()
				if Game.joueur_courant == -1:
					tab.insert(0, -s)
					tab.insert(1, nbCoupJoueSimu)
					return tab
				else :
					tab.insert(0, s)
					tab.insert(1, nbCoupJoueSimu)
					return tab
		Game.color *= -1
		if Game.color == 1:
			Game.joueur_courant = Game.noir
		else:
			Game.joueur_courant = Game.blanc
		Game.goban.play(coord, True)
		nbCoupJoueSimu += 1


class MonteCarlo(IA):

	values = 'MonteCarlo'
	nbCoupJoueTotal = 0
	nbSimulation = 0
	meilleurScore = 0
	timer = 0

	def donne_coup(self, game):
		joueur = game.joueur_courant
		debutChercheCoup = time.perf_counter()
		coord = self.lanceSimulation(game)
		finChercheCoup = time.perf_counter()
		MonteCarlo.timer += finChercheCoup - debutChercheCoup
		#print("durée coup : "+str(MonteCarlo.timer)+" secondes")
		game.joueur_courant = joueur
		#print("nombre de coup total : "+str(MonteCarlo.nbCoupJoueTotal))
		return coord

	def lanceSimulation(self, game):
		max_nb_evals = int(50)
		nbCoupJoueSimu = 0
		nbCoupJoue = 0
		score = []
		value = 0
		div = 0
		compt = 0
		noir = game.noir
		blanc = game.blanc
		for coup in game.goban.liste_coups_oks() :
			result = []
			for i in range(0,max_nb_evals):
				scoreInter = []
				tempGame = game.copy()
				#debutSimu = time.perf_counter()
				simu = simulation(tempGame,coup,nbCoupJoueSimu)
				#finSimu = time.perf_counter()
				#print("durée simulation : "+str(finSimu - debutSimu)+" secondes")
				MonteCarlo.nbSimulation += 1
				result.insert(i*2,simu)
			for resultat in result :
				value = value + resultat[0]
				nbCoupJoue += resultat[1]
				#print("nombre de coup joue : "+str(nbCoupJoue))
				#print("nombre de coup joue dans la simulation: "+str(resultat[1]))
				div += 1
			if coup != '-1' :
				scoreInter.insert(0,coup)
				scoreInter.insert(1,value/div)
				score.insert(compt,scoreInter)
			compt += 1 
		meilleur = -100.0
		index = 0
		compteur=0
		j=0
		while compteur < compt-1 :
			temp = float(score[j][1])
			if temp > meilleur :
				meilleur = score[j][1]
				index = score[j][0]
			j+=1
			compteur+=1
		if index == 0 :
			index = -1
		game.noir=noir
		game.blanc=blanc
		MonteCarlo.meilleurScore = meilleur
		MonteCarlo.nbCoupJoueTotal += nbCoupJoue
		return index


class Noeud() :
	
	def __init__(self, nbEval, evalMoy, listeEnfantEval):
		self.nbEval = nbEval
		self.evalMoy = evalMoy
		self.listeEnfantEval = listeEnfantEval

	def expandTree(self,game):
		self.listeEnfantEval = game.goban.liste_coups_oks()

	def simulation(self,game):
		invert_reward = True
		while True:
			if self.listeEnfantEval == []:
				reward = self.evalMoy
				return 1 - reward if invert_reward else reward
			self.simulation.__dict__ = Random.donne_coup('',game)
			invert_reward = not invert_reward

	def ancetre(self,reward):
		for noeud in self.listeEnfantEval:
			self.evalMoy[noeud] += 1
			self.nbEval[noeud] += reward
			reward = 1 - reward  # 1 for me is 0 for my enemy, and vice versa

	def _uct_select(self, node):
		"Select a child of node, balancing exploration & exploitation"

		# All children of node should already be expanded:
		assert all(n in self.listeEnfantEval for n in self.listeEnfantEval[node])

		log_N_vertex = math.log(self.nbEval[node])

		def uct(n):
			"Upper confidence bound for trees"
			return self.evalMoy[n] / self.nbEval[n] + math.sqrt(
				log_N_vertex / self.nbEval[n]
			)

		return max(self.listeEnfantEval[node], key=uct)
